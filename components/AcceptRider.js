import React from 'react';
import { Alert, AppRegistry, Button, StyleSheet, Image, Picker, Text, View } from 'react-native';

export default class AcceptRider extends React.Component {
  constructor(props) {
    super(props);

    // start is the starting location name, and end is the ending location name.
    this.state = {
      start: '',
      end: '',
    };
  }
  // ***needs to handle if value is not set.***
  // Create two pickers that set starting and ending locations.
  render() {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Button
          onPress={() => {
            Alert.alert(
              'Found a Rider!',

                'wants to ride with you!' + '\n' + ' /5 Stars' + '\n' + 'Pick up :' + '\n' + 'Drop off by:' + '\n' + 'Arrive by:',
              [
                {text: 'DECLINE', onPress: () => console.log('DECLINE Pressed'), style: 'cancel'},
                {text: 'ACCEPT', onPress: () => console.log('ACCEPT Pressed')},
              ],
              { cancelable: false }
            )
          }}
          title="Button"
        />
      </View>
    );
  }
}
