import React from 'react';
import {
    StyleSheet,
    Image,
    Picker,
    Text,
    View
} from 'react-native';

export default class ETAForm extends React.Component {
    constructor(props) {
        super(props);

        // start is the starting location name, and end is the ending location name.
        this.state = {
            start: '',
            end: '',
        };
    }

    // Create two pickers that set starting and ending locations.
    render() {
        return (
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                <Picker
                    selectedValue={this.state.start}
                    style={{ height: 200, width: 400 }}
                    onValueChange={(itemValue) => this.setState({start: itemValue})}>
                    <Picker.Item label="Will's House" value="will"  />
                    <Picker.Item label="Price Center" value="pc"  />
                    <Picker.Item label="Pangea Parking Structure" value="pps"  />
                </Picker>
                <Text style = {{ height: 25, width: 200}}>{this.state.start}</Text>
                <Picker
                    selectedValue={this.state.end}
                    style={{ height: 200, width: 400 }}
                    onValueChange={(itemValue) => this.setState({end: itemValue})}>
                    <Picker.Item label="Will's House" value="will"  />
                    <Picker.Item label="Price Center" value="pc"  />
                    <Picker.Item label="Pangea Parking Structure" value="pps"  />
                </Picker>
                <Text style = {{ height: 25, width: 200}}>{this.state.end}</Text>
            </View>
        );
    }
}