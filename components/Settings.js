import React from "react";
import {
  Alert,
  AppRegistry,
  Button,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Image,
  Picker,
  Text,
  TextInput,
  ScrollView,
  View,
  KeyboardAvoidingView,
  SafeAreaView
} from "react-native";
import { Header } from "react-native-elements";
import settings from "./../utilities/settings";
// import Navigation from "react-native-navigation";

export default class Settings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      oldPassword: "",
      newPassword: "",
      newPhoneNumber: "",
      make: "",
      model: "",
      color: "",
      numOfSeats: "",
      licensePlate: ""
    };
  }
  //TextInput: email
  //PasswordInputText: password
  //Button: Login
  //Button: RegisterAccount

  render() {
    return (
      <View style={{backgroundColor:"#fff", flex: 1}}>
          <SafeAreaView style={{backgroundColor:"#41A9E4", flex: 0}}/>
          <SafeAreaView style={{backgroundColor:"#fff", flex: 1}}>
          <Header
            leftComponent={{
              icon: "menu",
              size: 30,
              color: "#fff",
              // TODO: Make onpress open drawer
              onPress: () => this.props.navigation.openDrawer()
            }}
            centerComponent={{
              text: "Settings",
              style: { color: "#fff", fontSize: 26 }
            }}
            backgroundColor="#41A9E4"
          />
              <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.formContainer}>
                  <ScrollView style={styles.scroll}>
                    <Text style={styles.title}>Password</Text>
                    <TextInput
                      placeholder="Old Password"
                      secureTextEntry
                      placeholderTextColor="#11D0F9"
                      onChangeText={oldPassword => this.setState({ oldPassword })}
                      style={styles.input}
                    />
                    <TextInput
                      placeholder="New Password"
                      secureTextEntry
                      placeholderTextColor="#11D0F9"
                      onChangeText={newPassword => this.setState({ newPassword })}
                      style={styles.input}
                    />
                    <TouchableOpacity
                      onPress={() => {
                        if (this.state.oldPassword === "") {
                          Alert.alert(
                            "Invalid Input",
                            "Please enter your old password!"
                          );
                        } else if (this.state.newPassword === "") {
                          Alert.alert("Invalid Input", "Please enter a new password!");
                        } else {
                          this.handleChangePassword();
                        }
                      }}
                    >
                      <View style={styles.buttoncontainer}>
                        <Text style={styles.buttonText}>CHANGE PASSWORD</Text>
                      </View>
                    </TouchableOpacity>

                    <Text style={styles.title}>Contact</Text>

                    <TextInput
                      placeholder="New Phone Number"
                      placeholderTextColor="#11D0F9"
                      onChangeText={newPhoneNumber => this.setState({ newPhoneNumber })}
                      style={styles.input}
                    />
                    <TouchableOpacity onPress={() => this.handleChangePhone()}>
                      <View style={styles.buttoncontainer}>
                        <Text style={styles.buttonText}>UPDATE PHONE</Text>
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.title}>Driver Info</Text>
                    <TextInput
                      placeholder="Make"
                      placeholderTextColor="#11D0F9"
                      onChangeText={make => this.setState({ make })}
                      style={styles.input}
                    />
                    <TextInput
                      placeholder="Model"
                      placeholderTextColor="#11D0F9"
                      onChangeText={model => this.setState({ model })}
                      style={styles.input}
                    />
                    <TextInput
                      placeholder="Color"
                      placeholderTextColor="#11D0F9"
                      onChangeText={color => this.setState({ color })}
                      style={styles.input}
                    />
                    <TextInput
                      placeholder="Number of Seats"
                      placeholderTextColor="#11D0F9"
                      keyboardType="numeric"
                      onChangeText={numOfSeats => this.setState({ numOfSeats })}
                      style={styles.input}
                    />
                    <TextInput
                      placeholder="License Plate"
                      placeholderTextColor="#11D0F9"
                      onChangeText={licensePlate => this.setState({ licensePlate })}
                      style={styles.input}
                    />
                    <TouchableOpacity
                      onPress={() => {
                        if (this.state.make === "") {
                          Alert.alert("Invalid Input", "Make field is blank!");
                        } else if (this.state.model === "") {
                          Alert.alert("Invalid Input", "Model field is blank!");
                        } else if (this.state.color === "") {
                          Alert.alert("Invalid Input", "Color field is blank!");
                        } else if (this.state.numOfSeats === "") {
                          Alert.alert(
                            "Invalid Input",
                            "Number of Seats field is blank!"
                          );
                        } else if (this.state.licensePlate === "") {
                          Alert.alert("Invalid Input", "License Plate field is blank!");
                        } else {
                          this.handleChangeDriverInfo();
                        }
                      }}
                    >
                      <View style={styles.buttoncontainer}>
                        <Text style={styles.buttonText}>UPDATE DRIVER INFO</Text>
                      </View>
                    </TouchableOpacity>
                  </ScrollView>
                </View>
              </KeyboardAvoidingView>
          </SafeAreaView>
      </View>
    );
  }

  // Handling button clicks on Settings page
  async handleChangePassword() {
    if (
      await settings.updatePassword(
        this.props.navigation.state.params.email,
        this.state.newPassword,
        this.state.oldPassword
      )
    ) {
      Alert.alert("Success!", "Password changed.");
      // this.props.navigation.navigate("Home", { email: this.state.email });
    } else {
      Alert.alert("Incorrect password!", "Old password is incorrect.");
    }
  }

  async handleChangePhone() {
    await settings.updatePhoneNumber(
      this.props.navigation.state.params.email,
      this.state.newPhoneNumber
    );
    Alert.alert("Success!", "Phone number changed.");
  }

  async handleChangeDriverInfo() {
    await settings.updateCarInfo(
      this.props.navigation.state.params.email,
      this.state.color,
      this.state.make,
      this.state.model,
      this.state.numOfSeats,
      this.state.licensePlate
    );
    Alert.alert("Success!", "Car info changed.");
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  formContainer: {
    padding: 20
  },

  input: {
    height: 40,
    backgroundColor: "#E7FBFF",
    marginBottom: 20,
    paddingHorizontal: 10,
    color: "#11D0F9",
    borderRadius: 10
  },
  buttoncontainer: {
    height: 55,
    backgroundColor: "#06B7FB",
    alignItems: "center",
    margin: 20,
    fontWeight: "700",
    justifyContent: "center",
    borderRadius: 10
  },

  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
  },
  title: {
    color: "#000",
    width: 280,
    textAlign: "left",
    fontSize: 28,
    fontWeight: "800",
    marginBottom: 15,
  },

  scroll: {
  }
});
