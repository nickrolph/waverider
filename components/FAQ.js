import React from 'react';
import {
    StyleSheet,
    Image,
    Text,
    Button,
    View,
    TouchableOpacity
} from 'react-native';

import {
    Icon,
    Header,
} from 'react-native-elements';

import {
    SafeAreaView,
    ScrollView,
} from 'react-navigation';

export default class FAQ extends React.Component {
  render() {
      return (
        <View style={{backgroundColor:"#fff", flex: 1}}>
          <SafeAreaView style={{backgroundColor:"#41A9E4", flex: 0}}/>
          <SafeAreaView style={{backgroundColor:"#fff", flex: 1, marginBottom: 20}}>
              <View>
                  <Header
                  leftComponent={{
                    icon: 'menu',
                    size: 30,
                    color: '#fff',
                    // TODO: Make onpress open drawer
                    onPress: () => this.props.navigation.openDrawer()
                  }}
                      centerComponent={{
                          text: 'FAQ',
                          style: {
                              color: "#fff",
                              fontSize: 26
                          }
                      }}
                      backgroundColor='#41A9E4'
                  />
              </View>
              <View style={styles.formContainer}>
                  <ScrollView style={styles.scroll}>
                      <Text style={styles.header}>About</Text>
                      <Text style={styles.title}>What is WaveRider?</Text>
                      <Text style={styles.body}>WaveRider is a ride sharing solution made for Tritons
                                                by Tritons. WaveRirder makes it easy for Tritons to
                                                find others that are headed to the same destination.
                      </Text>
                      <Text style={styles.title}>Who made WaveRider?</Text>
                      <Text style={styles.body}>WaveRider is made by P.E.T.M.E., a group of young and enthusiastic
                                                UCSD undergraduates.
                      </Text>
                      <Text style={styles.header}>Questions</Text>
                      <Text style={styles.title}>Why Can't I Start a Ride?</Text>
                      <Text style={styles.body}>You cannot start a ride because you have not filed out all
                                                your driver information.
                      </Text>
                      <Text style={styles.title}>Why Haven't I Found a Ride?</Text>
                      <Text style={styles.body}>Unfortunately, no one currently is heading to the same destination as you.
                      </Text>
                      <Text style={styles.title}>What Happens if my Rider Doesn't Show?</Text>
                      <Text style={styles.body}>If your Rider doesn't show and fails to contact you, then you are free to
                                                head to your next destination.
                      </Text>
                  </ScrollView>
              </View>
          </SafeAreaView>
        </View>
      );
  }
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  backgroundColor: "#FFF"
},
formContainer: {
  padding: 20
},

header: {
  color: "#000",
  width: 280,
  textAlign: "left",
  fontSize: 36,
  fontWeight: "800",
  marginBottom: 15,
},

title: {
  color: "#3c3c3c",
  width: 280,
  textAlign: "left",
  fontSize: 28,
  fontWeight: "800",
  marginBottom: 15,
},

body: {
  color: '#666',
  width: 380,
  textAlign: "left",
  fontSize: 20,
  fontWeight: "800",
  marginBottom: 15,
},

scroll: {
}
});
