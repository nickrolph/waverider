import React from 'react';
import {
    FlatList,
    StyleSheet,
    Image,
    Picker,
    Text,
    Button,
    View,
    TouchableOpacity
} from 'react-native';
import {
    Icon,
    Header,
    ListItem
} from 'react-native-elements';

import {
    createDrawerNavigator,
    createAppContainer,
    DrawerItems,
    SafeAreaView,
    ScrollView,
    StackNavigator,
    DrawerNavigator,
    NavigationActions
} from 'react-navigation';

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            //expects
            data: [],
            refreshing: false,

            //parameters pulled from Home
            destination: this.props.navigation.state.params.destination,
            time: this.props.navigation.state.params.time,
            date: this.props.navigation.state.params.date,
            user: this.props.navigation.state.params.user,
        };
    }

    static navigationOptions = {
        drawerLabel: 'Home'
    }

    render() {
        return (
          <View style={{backgroundColor:"#fff", flex: 1}}>
            <SafeAreaView style={{backgroundColor:"#41A9E4", flex: 0}}/>
            <SafeAreaView style={{backgroundColor:"#fff", flex: 1}}>
                <Header
                    leftComponent={{
                      icon: 'menu',
                      size: 30,
                      color: '#fff',
                      // TODO: Make onpress open drawer
                      onPress: () => this.props.navigation.goBack()
                    }}
                    centerComponent={{ text: 'View Ride', style: { color: 'white',fontSize: 26}}}
                    backgroundColor='#41A9E4'
                />
                <View style={{flex: 1}}>
                    <View style={{flex: 1, alignItems: 'left', padding: 10}}>
                        <Text style={styles.userText}>{this.state.user}.</Text>
                        <Text style={styles.destinationText}>{this.state.destination}</Text>
                        <Text style={styles.bodyText}>Arrive by {this.state.time} on {this.state.date}</Text>
                        <GetUser isDriver={this.state.user}/>
                    </View>
                </View>
            </SafeAreaView>
          </View>
        );
    }
};

//Chooses which information to display
function GetUser(props){

  const isDriver = props.isDriver;

  if(isDriver === 'rider'){
    return <RiderReponse />;
  }
    else {
      return <DriverReponse />;
    }
}


function DriverReponse(prop){
  return <Text style={styles.riderTitle}>Your Riders:</Text>;
}

function RiderReponse(prop){
  return <Text style={styles.riderTitle}>Your Driver:</Text>;
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 0 ,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
        //     position: 'absolute',
        //     bottom: 0,
    },

    buttoncontainer: {
        height: 55,
        width: 150,
        backgroundColor: '#FFE9AF',
        margin: 5,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },

    userText: {
        textAlign: 'center',
        color: 'black',
        fontSize: 150,
    },

    destinationText: {
        textAlign: 'center',
        color: '#41A9E4',
        fontSize: 35,
    },

    bodyText: {
        textAlign: 'center',
        color: 'gray',
        fontSize: 25,
    },

    riderTitle: {
        textAlign: 'center',
        color: 'gray',
        fontSize: 25,
        paddingTop: 10
    },

    ridersText: {
      textAlign: 'center',
      color: 'gray',
      fontSize: 30,
    }

})
