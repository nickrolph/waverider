import React from 'react';
import {
    StyleSheet,
    Image,
    Picker,
    Text,
    Button,
    View,
    TouchableOpacity
} from 'react-native';

import {
    Icon,
    Header,
    ListItem
} from 'react-native-elements';

import {
    createDrawerNavigator,
    createAppContainer,
    DrawerItems,
    SafeAreaView,
    ScrollView,
    StackNavigator,
    DrawerNavigator
} from 'react-navigation';



export default class PastRides extends React.Component {
    constructor(props) {
        super(props);

        // this.state = {
        // };
    }


    render() {
        return (
          <View style={{backgroundColor:"#fff", flex: 1}}>
            <SafeAreaView style={{backgroundColor:"#41A9E4", flex: 0}}/>
            <SafeAreaView style={{backgroundColor:"#fff", flex: 1}}>
                <View>
                    <Header
                    leftComponent={{
                      icon: 'menu',
                      size: 30,
                      color: '#fff',
                      // TODO: Make onpress open drawer
                      onPress: () => this.props.navigation.openDrawer()
                    }}
                        centerComponent={{
                            text: 'Past Rides',
                            style: {
                                color: "#fff",
                                fontSize: 26
                            }
                        }}
                        backgroundColor='#41A9E4'
                    />

                    <Text style={styles.bodyText}> You have no past rides! </Text>
                </View>
            </SafeAreaView>
          </View>
        );
    }
};

const styles = StyleSheet.create({
    bodyText: {
        padding: 20,
        fontSize: 25,
        textAlign: 'center',
        color: 'gray'
    }
});
