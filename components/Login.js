import React from "react";
import Navigation from "react-native-navigation";
import {
  Alert,
  AppRegistry,
  Button,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Image,
  Picker,
  Text,
  TextInput,
  View,
  KeyboardAvoidingView
} from "react-native";
import login from "./../utilities/login";

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };
  }
  //TextInput: email
  //PasswordInputText: password
  //Button: Login
  //Button: RegisterAccount

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View style={styles.logocontainer}>
          <Image
            style={styles.logo}
            source={require("../assets/waverider_janky.png")}
          />
        </View>
        <View style={styles.formContainer}>
          <TextInput
            placeholder="Email@ucsd.edu"
            placeholderTextColor="#11D0F9"
            onChangeText={email => this.setState({ email })}
            style={styles.input}
          />
          <TextInput
            placeholder="Password"
            secureTextEntry
            placeholderTextColor="#11D0F9"
            onChangeText={password => this.setState({ password })}
            style={styles.input}
          />
          <TouchableOpacity onPress={() => this.handlePress()}>
            <View style={styles.buttoncontainer}>
              <Text style={styles.buttonText}>LOGIN</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("RegisterAccount")}
          >
            <View style={styles.createAccountContainer}>
              <Text style={styles.accountButtonText}>MAKE AN ACCOUNT</Text>
            </View>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
  async handlePress() {
    if (await login.login(this.state.email, this.state.password)) {
      this.props.navigation.navigate("Home", { email: this.state.email });
      this.props.setParams("Settings", { email: this.state.email }); // Pass email into Settings page
    } else {
      Alert.alert(
        "Invalid Input",
        "Email or Password is Wrong! Please Re-Enter Fields."
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  logocontainer: {
    alignItems: "center",
    flexGrow: 1,
    justifyContent: "center"
  },
  logo: {
    width: 325,
    height: 120
  },

  formContainer: {
    padding: 20,
    flex: 1
  },

  input: {
    height: 40,
    backgroundColor: "#E7FBFF",
    marginBottom: 20,
    paddingHorizontal: 10,
    color: "#11D0F9",
    borderRadius: 10
  },
  buttoncontainer: {
    height: 55,
    backgroundColor: "#06B7FB",
    alignItems: "center",
    marginBottom: 20,
    fontWeight: "700",
    justifyContent: "center",
    borderRadius: 10
  },

  createAccountContainer: {
    height: 55,
    backgroundColor: "#FFF",
    alignItems: "center",
    marginBottom: 20,
    fontWeight: "700",
    justifyContent: "center"
  },

  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
    padding: 20
  },

  accountButtonText: {
    textAlign: "center",
    color: "#06B7FB",
    padding: 20
  }
});
