import React from 'react';
import { Alert, AppRegistry, Button, StyleSheet, Image, Picker, Text, TextInput, View } from 'react-native';

export default class ProfilePicture extends React.Component {
    constructor(props) {
        super(props);

        // start is the starting location name, and end is the ending location name.
        this.state = {
            start: '',
            end: '',
        };
    }

    // ***needs to handle if value is not set.***
    // Create two pickers that set starting and ending locations.
    render() {
        return (
            <View style={{padding: 10}}>
                <Text>Set Profile Picture</Text>
                <Button
                    onPress={() => this.props.navigation.navigate('Login')}
                    title="Create Account"
                />
            </View>
        );
    }
}
