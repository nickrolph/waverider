import React from 'react';
import {
    StyleSheet,
    Image,
    Picker,
    Text,
    View,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
    ScrollView
} from 'react-native';
import {
    Header,
    ListItem
} from 'react-native-elements'
import { NavigationActions } from 'react-navigation';
import driverInfo from './../utilities/driverInfo';
import rides from '../utilities/rides';

const driverIcon = require('../assets/driver.png');
const riderIcon = require('../assets/rider.png');

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        // Not sure if we need this rn
        this.state = {
            start: '',
            end: '',
        };
    }

    static navigationOptions = {
        drawerLabel: 'Home'
    }

    render() {
        console.log(this.props.navigation.state.params.email);
        return (
          <View style={{backgroundColor:"#fff", flex: 1}}>
            <SafeAreaView style={{backgroundColor:"#41A9E4", flex: 0}}/>
            <SafeAreaView style={{backgroundColor:"#fff", flex: 1}}>
                <Header
                    leftComponent={{
                        icon: 'menu',
                        size: 30,
                        color: '#fff',
                        // TODO: Make onpress open drawer
                        onPress: () => this.props.navigation.openDrawer()
                    }}
                    centerComponent={{ text: 'Current Rides', style: { color: '#fff',fontSize: 26} }}
                    backgroundColor='#41A9E4'
                />
                <View style={{flex: 1}}>
                    <ScrollView>
                        <FlatList
                        data={[
                            {
                                user: 'driver',
                                destination: 'Center Hall',
                                time: '12:30pm',
                                date: '12/14/18'
                            },
                            {   user: 'rider',
                                destination: "Gilman Parking Structure",
                                time: '9:45am',
                                date: '12/15/18'
                            }
                        ]}
                        renderItem={({item}) => (
                            //conditional statement
                            item.user == 'driver' ?
                                <ListItem
                                    title={
                                        <View>
                                            <Text>Destination: {item.destination}</Text>
                                            <Text>Arrive by: {item.time}</Text>
                                            <Text>{item.date}</Text>
                                        </View>
                                    }
                                    leftIcon={
                                        <Image
                                            style={{
                                                width: 75,
                                                height: 75,
                                                margin: 10
                                            }}
                                            source={driverIcon}
                                        />
                                    }
                                    onPress={() => this.props.navigation.navigate('ViewRide', {destination: item.destination,
                                                                                                time: item.time,
                                                                                                date: item.date,
                                                                                                user: item.user
                                                                                                })}
                                />
                                :

                                <ListItem
                                    title={
                                        <View>
                                            <Text>Destination: {item.destination}</Text>
                                            <Text>Arrive by: {item.time}</Text>
                                            <Text>{item.date}</Text>
                                        </View>
                                    }

                                    leftIcon={
                                        <Image
                                            style={{
                                                width: 75,
                                                height: 75,
                                                margin: 10,
                                            }}
                                            source={riderIcon}
                                        />
                                    }

                                    onPress={() => this.props.navigation.navigate('ViewRide', {destination: item.destination,
                                                                                                time: item.time,
                                                                                                date: item.date,
                                                                                                user: item.user
                                                                                                })}

                                />
                        )}
                        //Gets rid of warning
                        keyExtractor={(item, index) => index.toString()}
                    />
                    </ScrollView>
                    <View style={ styles.MainContainer }>
                        <TouchableOpacity onPress={() => this.handleCreateRidePress()}>
                            <View style={styles.buttoncontainer1}>
                                <Text style={styles.buttonText1}>Create a Ride</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('RiderForm')}>
                            <View style={styles.buttoncontainer2}>
                                <Text style={styles.buttonText2}>Find a Ride</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
          </View>
        );
    }
    async handleCreateRidePress()
    {
      if(await driverInfo.getDriverStatus(this.props.navigation.state.params.email))
      {
        console.log('trueee');
        this.props.navigation.navigate('DriverForm', {email:this.props.navigation.state.params.email});
      }
      else
      {
        console.log('about to navigate to page');
        this.props.navigation.navigate('PromptDriverInfo', {email:this.props.navigation.state.params.email});
      }
    }
};

const styles = StyleSheet.create({
    MainContainer: {
        flex: 0 ,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
        //     position: 'absolute',
        //     bottom: 0,
    },

    buttoncontainer1: {
        height: 55,
        width: 325,
        backgroundColor: '#78d6ff',
        margin: 5,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },

    buttoncontainer2: {
        height: 55,
        width: 325,
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#78d6ff',
        margin: 5,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },

    buttonText1: {
        textAlign: 'center',
        color: 'white',
        fontSize: 25,
    },

  buttoncontainer: {
      height: 55,
      width: 150,
      backgroundColor: "#ffd556",
      margin: 5,
      borderRadius: 15,
      alignItems: "center",
      justifyContent: "center",
      shadowColor: "black",
      shadowOffset: { width: 0, height: 4 },
      shadowOpacity: 0.8
  },
    buttonText2: {
        textAlign: 'center',
        color: '#78d6ff',
        fontSize: 25,
    },
});
