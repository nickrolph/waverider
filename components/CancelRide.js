import React from 'react';
import { Alert, AppRegistry, Button, StyleSheet, Image, Picker, Text, View } from 'react-native';

export default class CancelRide extends React.Component {
  constructor(props) {
    super(props);

    // start is the starting location name, and end is the ending location name.
    this.state = {
      start: '',
      end: '',
    };
  }
  // ***needs to handle if value is not set.***
  // Create two pickers that set starting and ending locations.
  render() {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <Text>Pickup/Drop on Locations:</Text>

        <Text>Destination:</Text>
        <Text>Desired arrival time:</Text>
        <Text>Current Riders:</Text>
        <Button
          onPress={() => {
            Alert.alert('You tapped the button!');
          }}
          title="CANCEL RIDE"
          color="#841584"
        />
      </View>
    );
  }
}
