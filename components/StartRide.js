import React from 'react';
import { Alert, AppRegistry, Button, StyleSheet, Image, Picker, Text, View } from 'react-native';

export default class AcceptRider extends React.Component {
  constructor(props) {
    super(props);

    // start is the starting location name, and end is the ending location name.
    this.state = {
      start: '',
      end: '',
    };
  }
  // ***needs to handle if value is not set.***
  // Create two pickers that set starting and ending locations.
  render() {
    return (
      <View style={{backgroundColor:"#fff", flex: 1}}>
        <SafeAreaView style={{backgroundColor:"#41A9E4", flex: 0}}/>
        <SafeAreaView style={{backgroundColor:"#fff", flex: 1}}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Button
            onPress={() => {
              Alert.alert(
                'Start Ride!',

                'Your ride is starting in ' + {Timer} + '\n' + "You're headed to:" + '\n' + 'Stopping By:' + '\n' + 'At:',
                [
                  {text: 'START', onPress: () => console.log('START Pressed')},
                ],
                { cancelable: false }
              )
            }}
            title="Button"
          />
        </View>
        </SafeAreaView>
      </View>
    );
  }

}
const Timer = () => (
    <TimerCountdown
      initialSecondsRemaining={1000 * 60}
      onTick={secondsRemaining => console.log("tick", secondsRemaining)}
      onTimeElapsed={() => console.log("complete")}
      allowFontScaling={true}
      style={{ fontSize: 20 }}
    />
  );
