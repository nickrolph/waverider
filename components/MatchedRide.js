import React from 'react';
import {
    StyleSheet,
    Image,
    Text,
    Button,
    View,
    TouchableOpacity
} from 'react-native';
import {
    Icon,
    Header,
    ListItem
} from 'react-native-elements';
import {
    Navigation
} from 'react-navigation';


export default class MatchedRide extends React.Component {
    constructor(props) {
        super(props);

        // this.state = {
        // };

    }

    render() {
        let name = this.props.navigation.state.params.name;
        let phone = this.props.navigation.state.params.phone;
        let color = this.props.navigation.state.params.color;
        let make = this.props.navigation.state.params.make;

        return (
            <View>
                <Header
                    leftComponent={
                        <Icon
                            name='chevron-left'
                            type='entypo'
                            color='#fff'
                            onPress={() => this.props.navigation.goBack()}
                        />
                    }
                    centerComponent={{
                        text: 'Found a ride!',
                        style: {
                            color: "#fff",
                            fontSize: 26
                        }
                    }}
                    backgroundColor='#41A9E4'
                />

                <View style={styles.textContainer}>
                    <Text style={styles.title}> {name} will pick you up! </Text>
                    <Text> Phone number: {phone} </Text>
                    <Text> Car: {color}  {make} </Text>
                </View>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                    <View style={styles.buttonContainer}>
                        <Text style={styles.buttonText}>OK!</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    textContainer: {
        padding: 20,
    },
    title: {
        fontSize: 30,
        textAlign: 'center'
    },
    buttonContainer: {
        height: 55,
        width: 325,
        backgroundColor: '#78d6ff',
        margin: 5,
        borderRadius: 15,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize: 25,
    }
});


