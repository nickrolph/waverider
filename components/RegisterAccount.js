import React from "react";
import Navigation from "react-native-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  Alert,
  AppRegistry,
  Button,
  StyleSheet,
  Image,
  Picker,
  Text,
  TextInput,
  View,
  TouchableOpacity
} from "react-native";

//import login from './../utilities/login';
//{login(xyz)}
import login from "./../utilities/login";

export default class RegisterAccount extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      password: "",
      confirmPassword: ""
    };
  }
  static navigationOptions = {
    title: "RegisterAccount"
  };
  //TextInput: firstName
  //TextInput: lastName
  //TextInput: email
  //NumInput: phoneNumber
  //PasswordInputText: firstPassword
  //PasswordInputText: secondPassword
  //Button: Next
  render() {
    return (
      <KeyboardAwareScrollView behavior="padding" style={styles.container}>
        <View style={styles.logocontainer}>
          <Image
            style={styles.logo}
            source={require("../assets/waverider_janky.png")}
          />
        </View>
        <View style={styles.formContainer}>
          <Text style={styles.title}>Account Information</Text>
          <TextInput
            placeholder="First Name"
            placeholderTextColor="#11D0F9"
            onChangeText={firstName => this.setState({ firstName })}
            style={styles.input}
          />
          <TextInput
            placeholder="Last Name"
            placeholderTextColor="#11D0F9"
            onChangeText={lastName => this.setState({ lastName })}
            style={styles.input}
          />
          <TextInput
            placeholder="UCSD Email"
            placeholderTextColor="#11D0F9"
            onChangeText={email => this.setState({ email })}
            style={styles.input}
          />
          <TextInput
            placeholder="Phone Number                 (ex: 1234567890)"
            placeholderTextColor="#11D0F9"
            keyboardType="numeric"
            onChangeText={phoneNumber => this.setState({ phoneNumber })}
            style={styles.input}
          />
          <TextInput
            placeholder="Password"
            secureTextEntry
            placeholderTextColor="#11D0F9"
            onChangeText={password => this.setState({ password })}
            style={styles.input}
          />
          <TextInput
            placeholder="Confirm Password"
            secureTextEntry
            placeholderTextColor="#11D0F9"
            onChangeText={confirmPassword => this.setState({ confirmPassword })}
            style={styles.input}
          />
          <TouchableOpacity
            onPress={() => {
              if (this.state.firstName === "") {
                Alert.alert("Invalid Input", "Please Input a First Name!");
              } else if (this.state.lastName === "") {
                Alert.alert("Invalid Input", "Please Input a Last Name!");
              } else if (this.state.email === "") {
                Alert.alert("Invalid Input", "Please Input a valid UCSD E-Mail!");
              } else if ((this.state.email.split("@").length - 1) != 1 ||
                          this.state.email.split("@")[1] != "ucsd.edu") {
                Alert.alert("Invalid Input", "Please Input a valid UCSD E-Mail!");
              } else if (this.state.phoneNumber === "") {
                Alert.alert("Invalid Input", "Please Input a Phone Number!");
              } else if (this.state.password === "") {
                  Alert.alert("Invalid Input", "Please Input a Password!");
              } else if (this.state.phoneNumber.length != 10) {
                Alert.alert("Invalid Input", "Please Input a Phone Number (ex: 1234567890)");
              } else if (this.state.confirmPassword == "") {
                Alert.alert("Invalid Password", "Please Confirm Password!");
              } else if (this.state.password === this.state.confirmPassword) {
                this.handlePress();
              } else {
                Alert.alert("Invalid Password", "Passwords Do Not Match!");
              }
            }}
          >
            <View style={styles.buttoncontainer}>
              <Text style={styles.buttonText}>Next</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Login")}
          >
            <View style={styles.buttoncontainer}>
              <Text style={styles.buttonText}>Go Back</Text>
            </View>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    );
  }
  async handlePress() {
    if (
      await login.register(
        this.state.email,
        this.state.password,
        this.state.lastName,
        this.state.firstName,
        {},
        false,
        this.state.phoneNumber
      )
    ) {
      this.props.navigation.navigate("DriverInfo", { email: this.state.email });
    } else {
      Alert.alert("Invalid Input", "Email is Already In Use!");
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  logo: {
    marginTop: 30,
    width: 250,
    height: 100
  },
  logocontainer: {
    alignItems: "center",
    flexGrow: 1,
    justifyContent: "center"
  },
  title: {
    color: "#11D0F9",
    width: 280,
    textAlign: "left",
    fontSize: 28,
    fontWeight: "800",
    marginBottom: 15,
    padding: 20
  },
  formContainer: {
    padding: 20
  },

  input: {
    height: 40,
    backgroundColor: "#E7FBFF",
    marginBottom: 20,
    paddingHorizontal: 10,
    color: "#11D0F9",
    borderRadius: 10
  },
  buttoncontainer: {
    height: 55,
    backgroundColor: "#06B7FB",
    alignItems: "center",
    marginBottom: 20,
    fontWeight: "700",
    justifyContent: "center",
    borderRadius: 10
  },
  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
    padding: 20
  }
});
