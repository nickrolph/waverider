import React from "react";
import {
  Alert,
  AppRegistry,
  Button,
  DatePickerIOS,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  ScrollView
} from "react-native";
import { Header, Icon } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import { Moment } from "moment";
import login from "../utilities/login";
import search from "../utilities/search";
import ridesUtils from "../utilities/rides";

export default class RiderForm extends React.Component {
    constructor(props) {
        super(props);

        // start is the starting location name, and end is the ending location name.
        this.state = {
            start: '',
            end: '',
            selectedDate: new Date()
        };
    }

    //TODO: NEED TO FORMAT DATE MM/DD/YYYY
    changeDate(newDate) {
        //console.log(newDate.toISOString())
        // console.log(Moment(this.state.selectedDate).format('MMMM Do YYYY HH:mm'));
        this.setState({selectedDate: newDate})
    };

    // ***needs to handle if value is not set.***
    // Create two pickers that set starting and ending locations.
    render() {
        let pickUpLocations = [{
            value: "AXIOM La Jolla",
        }, {
            value: "La Jolla Villa Tennis Clubs",
        }, {
            value: "La Regencia",
        }, {
            value: "La Scala Apartment Homes",
        }, {
            value: "The Venetian Aparments",
        }, {
            value: "Westfield UTC",
        }];

        let dropOffLocations = [{
            value: "Center Hall",
        }, {
            value: "Gilman Parking Structure",
        }, {
            value: "Hopkins Parking Structure",
        }, {
            value: "Pangea Parking Structure",
        }, {
            value: "Pepper Canyon Hall",
        }, {
            value: "Price Center",
        }];



        return (
          <View style={{backgroundColor:"#fff", flex: 1}}>
            <SafeAreaView style={{backgroundColor:"#41A9E4", flex: 0}}/>
            <SafeAreaView style={{backgroundColor:"#fff", flex: 1}}>
            <View>
                <Header
                    leftComponent={
                        <Icon
                            name='chevron-left'
                            type='entypo'
                            color='#fff'
                            onPress={() => this.props.navigation.goBack()}
                        />
                    }
                    centerComponent={{
                        text: 'Find a Ride',
                        style: {
                            color: "#fff",
                            fontSize: 26
                        }
                    }}
                    backgroundColor='#41A9E4'
                />

                <View style={{padding: 20}}>
                    <View style={styles.container}>
                        <Dropdown
                            label='Pick up location'
                            data={pickUpLocations}
                            labelFontSize={20}
                            textColor="rgba(0, 0, 0, .70)"
                            itemColor="rgba(0, 0, 0, .24)"
                            onChangeText={(value) => this.setState({start: value})}
                        />
                    </View>
                    <View style={styles.container}>
                        <Dropdown
                            label='Drop off location'
                            data={dropOffLocations}
                            labelFontSize={20}
                            textColor="rgba(0, 0, 0, .70)"
                            itemColor="rgba(0, 0, 0, .24)"
                            onChangeText={(value) => this.setState({end: value})}
                        />
                    </View>


                    <View style={styles.timeContainer}>
                        <Text style={{fontSize: 20, color: 'rgba(0, 0, 0, .38)'}}>Desired arrival time:</Text>
                        <DatePickerIOS
                            date={this.state.selectedDate}
                            //TODO: SET STATE OF DATE AFTER CHANGING
                            onDateChange={(date) => this.changeDate(date)}
                            mode="datetime"

                        />

                        {/*console.log({this.state.selectedDate})*/}
                        {/*<View> {Moment(selectedDate).format('d MMM')} </View>*/}
                    </View>

                </View>

                {/*TODO: ADD ACTUAL FUNCTIONALITY AFTER PRESSING BUTTON*/}
                <TouchableOpacity onPress={() => this.handlePress()}>
                {/*<TouchableOpacity onPress={() =>  this.props.navigation.navigate('MatchedRide')}>*/}
                    <View style={styles.buttonContainer}>
                        <Text style={styles.buttonText}>Find a Ride</Text>
                    </View>
                </TouchableOpacity>
            </View>
            </SafeAreaView>
          </View>
        );
    }
    async handlePress() {
        let matchedDriver = await search.searchRide(this.state.end, this.state.start, this.state.selectedDate)

        console.log("This is ID: "+ matchedDriver);

        if (!matchedDriver) {
            // no match
            Alert.alert(
                "No match",
            "There are no available drivers at this time."
            );
        }
        else {
            //there is a match. get user dictionary
            let user = await ridesUtils.display(matchedDriver);
            console.log("This is the user color: " + user.color);
            this.props.navigation.navigate('MatchedRide', {color: user.color,
            make: user.make, plateNum: user.plateNum, phone: user.phoneNumber, name: user.firstName })
        }
    }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15
  },
  timeContainer: {
    marginTop: 30
  },
  buttonContainer: {

      height: 55,
      width: 325,
      backgroundColor: '#78d6ff',
      margin: 5,
      borderRadius: 15,
      alignSelf: 'center',
      justifyContent: 'center',
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 25,
  }
});
