import React from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';

export default class BlinkingGary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showGary: true
    };
    // Toggle the state every quarter second
    setInterval(() => (
      this.setState(previousState => (
        { showGary: !previousState.showGary }
      ))
    ), 250);
  }

  render() {
    if (!this.state.showGary) {
      return null;
    }

    return (
      <View style={{alignItems: 'center'}}>
        <Image
          source={require('../assets/big_gary.jpg')}
          style={{width: 69, height: 420}}
        />
      </View>
    );
  }
}
