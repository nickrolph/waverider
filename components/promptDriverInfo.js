import React from 'react';
import Navigation from 'react-native-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
    Alert,
    AppRegistry,
    Button,
    StyleSheet,
	StatusBar,
	TouchableOpacity,
    Image,
    Picker,
    Text,
    TextInput,
    View,
	KeyboardAvoidingView,
} from 'react-native';
import driverInfo from './../utilities/driverInfo';

export default class DriverInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            make: '',
            model: '',
            color: '',
            numOfSeats: '',
            licensePlate: '',
        };
    }
    static navigationOptions = {
        title: 'PromptDriverInfo'
    };
    //TextInput: make
    //TextInput: model
    //TextInput: color
    //NumInput: numOfSeats
    //TextInput: licensePlate
    //Button: Next
    render() {
        return (
			<KeyboardAwareScrollView  behavior="padding" style={styles.container}>
				<Text style={styles.title}> Car Information </Text>
				<Text style={styles.text}>You will need this information to be able to drive other students. </Text>
				<View style={styles.formContainer}>

					<TextInput
						placeholder= "Make"
						placeholderTextColor="rgba(255,255,255,0.7)"
						onChangeText={(make) => this.setState({make})}
						style={styles.input}
						/>
					<TextInput
						placeholder= "Model"
						placeholderTextColor="rgba(255,255,255,0.7)"
						onChangeText={(model) => this.setState({model})}
						style={styles.input}
						/>
					<TextInput
						placeholder= "Color"
						placeholderTextColor="rgba(255,255,255,0.7)"
						onChangeText={(color) => this.setState({color})}
						style={styles.input}
						/>
					<TextInput
						placeholder= "Number of Seats"
						placeholderTextColor="rgba(255,255,255,0.7)"
						keyboardType= 'numeric'
						onChangeText={(numOfSeats) => this.setState({numOfSeats})}
						style={styles.input}
						/>
					<TextInput
						placeholder= "License Plate"
						placeholderTextColor="rgba(255,255,255,0.7)"
						onChangeText={(licensePlate) => this.setState({licensePlate})}
						style={styles.input}
						/>
				</View>
				<View style={styles.formContainer}>
					<TouchableOpacity onPress={() => this.handlePress()}>
						<View style={styles.buttoncontainer}>
							<Text style={styles.buttonText}>Register Car</Text>
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
						<View style={styles.buttoncontainer}>
							<Text style={styles.buttonText}>Go Back</Text>
						</View>
					</TouchableOpacity>
				</View>

			</KeyboardAwareScrollView >
        );
    }
    async handlePress() {
          if(this.state.make == '' ||
            this.state.model == '' ||
            this.state.color == '' ||
            this.state.numOfSeats == '' ||
            this.state.licensePlate == ''
          )
          {
            Alert.alert('Driver Information', 'You will not be able to drive other students.');
            this.props.navigation.navigate('Home');
          }
          else {
              await driverInfo.updateDriverInfo(
              this.props.navigation.state.params.email,
              this.state.make,
              this.state.model,
              this.state.color,
              this.state.numOfSeats,
              this.state.licensePlate)
              Alert.alert('Updated Driver Information', 'You will now be able to drive other students.');
              this.props.navigation.navigate('DriverForm', {email:this.props.navigation.state.params.email})
          }

    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#3498db',
	},
	logocontainer: {


		justifyContent: 'center'
	},
	logo: {
		width: 180,
		height: 180,
	},
	title: {
		color: '#FFF',
		marginTop: 30,
		width: 280,
		textAlign: 'left',
		opacity: 0.8,
		fontSize: 24,
		fontWeight: '800',
		marginBottom: 20,
		padding: 20,
	},
	formContainer: {
		padding: 20,
	},

	input: {
		height: 40,
		backgroundColor: 'rgba(255,255,255,0.2)',
		marginBottom: 20,
		paddingHorizontal: 10,
		color: '#FFF',
	},
	buttoncontainer: {
		height: 55,
		backgroundColor: '#2980b9',
		alignItems: 'center',
		marginBottom: 20,
		fontWeight: '700',
		justifyContent: 'center',
	},
	buttonText: {
		textAlign: 'center',
		color: '#FFFFFF',
		padding: 20,
	},
	text: {
		height: 40,
		marginBottom: 20,
		paddingHorizontal: 20,
		color: '#FFF',
	}
})
