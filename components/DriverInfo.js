import React from "react";
import Navigation from "react-native-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  Alert,
  AppRegistry,
  Button,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Image,
  Picker,
  Text,
  TextInput,
  View,
  KeyboardAvoidingView
} from "react-native";
import driverInfo from "./../utilities/driverInfo";

export default class DriverInfo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      make: "",
      model: "",
      color: "",
      numOfSeats: "",
      licensePlate: ""
    };
  }
  static navigationOptions = {
    title: "DriverInfo"
  };
  //TextInput: make
  //TextInput: model
  //TextInput: color
  //NumInput: numOfSeats
  //TextInput: licensePlate
  //Button: Next
  render() {
    return (
      <KeyboardAwareScrollView behavior="padding" style={styles.container}>
        <Text style={styles.title}> Car Information </Text>
        <Text style={styles.text}>
          You will need this information to be able to drive other students. You
          can fill this information later.
        </Text>
        <View style={styles.formContainer}>
          <TextInput
            placeholder="Make"
            placeholderTextColor="#11D0F9"
            onChangeText={make => this.setState({ make })}
            style={styles.input}
          />
          <TextInput
            placeholder="Model"
            placeholderTextColor="#11D0F9"
            onChangeText={model => this.setState({ model })}
            style={styles.input}
          />
          <TextInput
            placeholder="Color"
            placeholderTextColor="#11D0F9"
            onChangeText={color => this.setState({ color })}
            style={styles.input}
          />
          <TextInput
            placeholder="Number of Seats"
            placeholderTextColor="#11D0F9"
            keyboardType="numeric"
            onChangeText={numOfSeats => this.setState({ numOfSeats })}
            style={styles.input}
          />
          <TextInput
            placeholder="License Plate"
            placeholderTextColor="#11D0F9"
            onChangeText={licensePlate => this.setState({ licensePlate })}
            style={styles.input}
          />
        </View>
        <View style={styles.formContainer}>
          <TouchableOpacity onPress={() => this.handlePress()}>
            <View style={styles.buttoncontainer}>
              <Text style={styles.buttonText}>Register Account</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("RegisterAccount")}
          >
            <View style={styles.buttoncontainer}>
              <Text style={styles.buttonText}>Go Back</Text>
            </View>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    );
  }
  async handlePress() {
    if (
      this.state.make == "" ||
      this.state.model == "" ||
      this.state.color == "" ||
      this.state.numOfSeats == "" ||
      this.state.licensePlate == ""
    ) {
      Alert.alert(
        "Registered Account",
        "You will not be able to drive other students until you fill out your driver information."
      );
    } else {
      await driverInfo.updateDriverInfo(
        this.props.navigation.state.params.email,
        this.state.make,
        this.state.model,
        this.state.color,
        this.state.numOfSeats,
        this.state.licensePlate
      );
      Alert.alert(
        "Registered Account",
        "You will be able to drive other students."
      );
    }
    this.props.navigation.navigate("Login");
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  logocontainer: {
    justifyContent: "center"
  },
  logo: {
    width: 250,
    height: 100
  },
  title: {
    color: "#11D0F9",
    marginTop: 30,
    width: 280,
    textAlign: "left",
    fontSize: 24,
    fontWeight: "800",
    marginBottom: 20,
    padding: 20
  },
  formContainer: {
    padding: 20
  },

  input: {
    height: 40,
    backgroundColor: "#E7FBFF",
    marginBottom: 20,
    paddingHorizontal: 10,
    color: "#11D0F9",
    borderRadius: 10
  },
  buttoncontainer: {
    height: 55,
    backgroundColor: "#06B7FB",
    alignItems: "center",
    marginBottom: 20,
    fontWeight: "700",
    justifyContent: "center",
    borderRadius: 10
  },
  buttonText: {
    textAlign: "center",
    color: "#FFFFFF",
    padding: 20
  },
  text: {
    height: 40,
    marginBottom: 20,
    paddingHorizontal: 20,
    color: "#11D0F9"
  }
});
