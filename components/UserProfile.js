import React from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';

export default class UserProfile extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>This is the temporary user profile!</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
