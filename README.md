### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Workflow

When creating a new feature or addition to the code base:

Create a new branch with YourInitials_FeatureName EX: nr_login_feature

Develop tests for your own code

Work on that branch push changes whenever you want

Pull changes from Master whenever you want

When ready to merge with master: pull chages from Master, fix merge conflicts, then push to your own branch

Submit merge request to master on Gitlab, assign it to Nicholas

I will review code and make comments on the request if changes are needed

## Use Expo to run app demo

### Install Expo
[Guide for installing Expo](https://docs.expo.io/versions/latest/introduction/installation)

Run the following command in command prompt to install Expo on your computer
```
npm install -g expo-cli
```
Also install the mobile client on your phone from your Android/IOS app store

### Run Expo
In waverider-master/waverider directory, do the following command
```
npm install
npm install react-native-keyboard-aware-scroll-view --save
npm install --save react-native-material-dropdown
```
### use Jest for unit tests
```
npm install jest
npm test
```
After installation, run the following command to start expo
```
expo start
```
Now a tab will pop up in your browser, you can either scan the QR code from
the browser tab or from the command prompt to run simulation on your phone

### PETME Introduction Video

https://www.youtube.com/watch?v=r4hjIizoPfA
