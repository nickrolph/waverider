const {db} = require('./db');
const moment = require('moment');

/*
    This method returns a list of rides that fit the query's standards.
 */
let searchRide = async (arrivalDestination, pickupLocation, arrivalTime) => {
    let ride = await db.collection("rides").where('dropOffPoint', '==', arrivalDestination)
    .where('pickUpPoints', '==', pickupLocation)
    .where('isDone', '==', false)
    .get()
    .then( (snapshot)=> {

        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }

        // Maximum bound for time difference of arrival times.
        const TIME_CUTOFF = Infinity;

        let temp;
        let arrivalMoment = moment(arrivalTime);

        // Set defaults for ride and diff to compare against.
        let diff = TIME_CUTOFF;
        snapshot.forEach(doc => {
            // The moment object of the ride being checked.
            let curMoment = moment(doc.data().date);

            // The difference between curMoment and the requested arrivalMoment.
            let curDiff = curMoment.diff(arrivalMoment);
            // THe difference from arrivalMoment to today.
            let todayDiff = moment().diff(curMoment);
            // Can't have an arrival time after the requested one.
            if (curDiff > 0) {
                // Arrival time compared to the other best one, and also the arrival time has to be after the current
                // time.
                if (diff > curDiff && todayDiff < 0) {
                    temp = doc.id;
                    diff = curDiff;
                }
            }
        });

        // Don't return if no viable rides were found.
        if (diff >= TIME_CUTOFF || temp == null) {
            console.log("No rides found after current date");
            return null;
        }

        return temp;
    }).catch(err => {
        console.log('Error getting rides', err);
    });
    return ride;
};

let temp = async () => {
    let ride = await searchRide("sasho's place", "will's house", moment().add(5,'days').format());
};
temp();

module.exports = {searchRide};