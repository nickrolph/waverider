import {db} from './db';
/*
    Expects user email, and car information from the user (make, model, color, number of seats, plate number)
    Updates the information in the database for that user
 */
async function updateDriverInfo(email, make, model, color, numSeats, plateNum) {
      email = email.toLowerCase();
      return await db.collection("users").where('email', "==", email)
        .get()
        .then(async (doc) => {
          await doc.forEach((docRef)=> {
            docRef.ref.update({
                        "carInfo": {
                        "make": make,
                        "model": model,
                        "color": color,
                        "numSeats": numSeats,
                        "plateNum": plateNum
                    },
                    "isDriver": true
                })
            })
            return true;
        }).catch((e)=>{
          console.log(e);
          return false;
        })
};

/*
    Function to get whether a user can drive or not (if driver info has been filled out)
    Expects user email and returns true or false
 */
async function getDriverStatus(email) {
    email = email.toLowerCase();
    return await db.collection("users").where('email', "==", email)
        .get()
        .then( async(doc) =>{
            let isDriver = false;
            await doc.forEach( async (docRef) => {
                isDriver = docRef.data().isDriver
            });
            return isDriver;
        })
}

/*
    Function to get whether a user can drive or not (if driver info has been filled out)
    Expects user email and returns true or false
 */
async function getNumSeats(email) {
    email = email.toLowerCase();
    return await db.collection("users").where('email', "==", email)
        .get()
        .then( async(doc) =>{
            let model = "";
            if (doc.empty) {
                console.log("No users match email.");
                return null;
            }
            await doc.forEach( async (docRef) => {
                model = docRef.data().carInfo.numSeats;
            });
            return model;
        })
}

export default {updateDriverInfo, getDriverStatus, getNumSeats};

