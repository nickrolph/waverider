import {db} from './db';
// const {db} = require('./db');
/*
    This login needs the input of email and password, searches the db for the user
    then uses another method I defined in auth.js to check if the password is the
    same as the hash.
    returns true if logged in and false if not
*/
async function login(email, password) {
  email = email.toLowerCase();
  let fair = await db.collection("users").where('email', "==",email)
      .get()
      .then(function(querySnapshot) {
          // SEARCHING FOR USER
          // THIS IS A PROMISE. I have to use the await keyword to make sure
          // sure that it finishes before moving on with the rest of our function
          // this is important because I rely on the contents of the variable "fair"
          // later on in the function
          // if there is a return in the .then() function, that is what will be stored
          // in the variable "fair"
          // doc.data() is never undefined for query doc snapshots

              let temp;
              querySnapshot.forEach(function(doc) {
                  //what I'm doing here is very important
                  // I need the contents of this forEach, but it is an asynchronous callback,
                  // meaning that I don't actually know when it will finish and can't
                  // save the contents before moving on. This does not take promises so I can't
                  //use the keyword await either. So I declared a new variable with a bigger scope
                  //called temp and stored the data I got inside the foreach Into that.
                  // this is a roundabout way to take data from asynchronous callbacks in Javascript.
                  //Also it's important to note, that this only worked because I return temp inside
                  //the .then() meaning fair turned into whatever temp was.
              if(!doc|| password!== doc.data().password){
                  temp = false;
                  return false;
              }
              temp = true;
              return true;
          });
          return temp;
      })
      .catch(e=> console.log(e));
      return fair;
};

/*
    This requires a lot of fields to work.  It first searches
    for the user by email and then hashes the password
    and sets all the fields

    returns true if it wokrs and false if it doesnt.
    It will return false if the email is already taken as well.
    ******* VERY IMPORTANT ********
    MAKE SURE TO GET THE ORDER OF VARIABLES CORRECT WHEN YOU CALL THIS METHOD
*/
async function register(email, password, lastName,firstName, carInfo, isDriver, phoneNumber){
  email = email.toLowerCase();
  console.log('email2', email);
  console.log('isDriver', isDriver);
  console.log('phoneNumber2', phoneNumber);
    return await db.collection("users").where('email', "==", email)
    .get()
    .then(async (doc)=>{
        // SEARCHING FOR USER
        // THIS IS A PROMISE. I return the result of this promise
        // as the answer, so I need to use 'await' on it, so I wait for it to finish.
        // to use await I have to declar my function using the keyword 'async'
        if(!doc.empty){
            return false;
        }
        else{


            return await db.collection("users").add({
                //Again, I'm returning the results of a promise, so I have to use the await keyword.
                //Keep in mind this is a callback, so even though I defined "register" async, I need to decalr it again
                // on line 18 so I can use the 'await' keyword inside of it.
                email: email,
                password: password,
                isDriver: isDriver,
                carInfo: carInfo,
                phoneNumber: phoneNumber,
                rides: [],
                lastName:lastName,
                firstName: firstName
            }).then((docRef)=>{

                return true;
            }).catch((error)=>{
                console.log("error adding: ", error);
            }
            );

        }
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
};

let temp = async ()=>{
    console.log(await login("wlxu@ucsd.edu","hi"));
    console.log(await register("wlxu@ucsd.edu",
    "hi",
    "Xu",
    "Will",
    {
        "color" : "white",
		"make": "honda",
        "model" : "accord",
        "numSeats" : 5,
        "plateNum" : "urmumisahoe"
    },
    true,
    "510-269-0349"));

}
// temp();

export default { login, register };
