const {db} = require('./db');
const moment = require('moment');

/*
    Method to return the current rides for a user from his email
    Parameter is user email for which to return current rides
    Returns array of ids of current rides for this user
 */
let get = async function getRides(email){
    email = email.toLowerCase();

    let currentRides = [];

    await db.collection("users").where('email', '==', email)
        .get()
        .then( (docUsers) =>{
            docUsers.forEach( async (userRef)=> {

                let user = userRef.data();
                let i = 0;
                for (i = 0; i < user.rides; i++) {

                    await db.collection("rides").doc(user.rides[i])
                        .get()
                        .then( (docRides) => {
                            docRides.forEach( (rideRef) => {
                                let ride = rideRef.data();

                                if (ride.isDone === false) {
                                    user.push(user.rides[i])
                                }
                            })
                        })
                }
            })
        });

    return currentRides
};

/*
    This method first adds a new ride to the db. Then it saves the id
    so we can store the id of the ride with the user who created it.
    It then searches for the user and adds the ride to the users
    ride list.

    Important that departureDay is in the form of a Javascript Date object.
    View temp method at bottom of file for an example.

    returns false if this failed
    will fail if points is empty

    Returns the ride ID
    ********************** IMPORTANT **********************
    Again, a lot of parameters, be SURE the order is correct
*/
let create = async function createRide(dropOffPoint, pickUpPoints, driverEmail, departureDay, numSeats){
    driverEmail = driverEmail.toLowerCase();
    let id;
    let temp;
    if(pickUpPoints.length){
        temp = await db.collection("rides").add({
            //again, I have a promise(asynchronous method) which has important info, so I need
            //to wait for this function to finish, and use the keyword await and declare the method
            //it's in as async
            driver: driverEmail,
            dropOffPoint: dropOffPoint,
            pickUpPoints: pickUpPoints,
            arrivalTime: null,
            numSeat: numSeats,
            isDone: false,
            riders:[],
            date : moment(departureDay).toISOString()
        }).then(async (docRef)=>{

            return docRef;

        }).catch(e=> console.log(e));
        await db.collection("users").where('email', "==", driverEmail)
        .get()
        .then( (doc)=>{
            doc.forEach(async (docRef)=>{
                //I need to use await for something in this method,
                //so I declared the callback again using "async"
                let prevRides = docRef.data().rides;
                let updateRef = docRef.ref;

                prevRides.push(temp.id);
                updateRef.update({
                    rides:prevRides
                })
            });
        }).catch((error)=>{
            console.log("error adding: ", error);
        });
        return temp.id;
    }
    else{
        return false;
    }
};

/*
    Function to display the driver information
    Returns user information in dictionary:

    {
        "firstName": first name
        "lastName": last name
        "color": color
        "make" : make
        "plateNum": plate number
        "phoneNumber": phone number
    }
 */
let display = async (rideID) => {

    let userInfo = {"color":"",
                    "make":"",
                    "plateNum":"",
                    "phoneNumber":"",
                    "firstName":"",
                    "lastName":""}

    await db.collection("rides").doc(rideID)
        .get()
        .then( async (snapshot) => {


            if (!snapshot.exists) {
                console.log("No match for ride ID")
                return;
            }

            let driver = snapshot.data().driver;
            driver = driver.toLowerCase();

            await db.collection("users").where("email", "==", driver)
            .get()
                .then( async (ref) => {

                    if (ref.empty) {
                        console.log("No match for driver ID")
                        return;
                    }
                    ref.forEach( (doc) => {
                        let userData = doc.data();
                        userInfo.color = userData.carInfo.color;
                        userInfo.make = userData.carInfo.make;
                        userInfo.plateNumber = userData.carInfo.plateNum;
                        userInfo.phoneNumber = userData.phoneNumber;
                        userInfo.firstName = userData.firstName;
                        userInfo.lastName = userData.lastName;
                    })
                })
        })

    return userInfo
}

/*
    This function allows a rider with the specified email to join a ride by ID if there is room in the ride.

    Places the email of the new rider into the riders field of the ride, and also adds
    the rideID to the rides field in the user. Fails if the ride is full or already completed.
 */
let join = async function joinRide(rideID, driverEmail) {
    driverEmail = driverEmail.toLowerCase();
    await db.collection("rides").doc(rideID)
    .get()
    .then( async (snapshot) => {
        if (!snapshot.exists) {
            console.log("No ride exists.");
        } else {
            let ride = snapshot.data();

            if (ride.riders.length >= ride.numSeat - 1) {
                console.log("Ride is full.");
                return;
            }

            if (ride.isDone) {
                console.log("Ride has already been completed.");
                return;
            }

            await db.collection("users").where('email', '==', driverEmail)
            .get()
            .then( async (userRef)=> {
                if (userRef.empty) {
                    console.log("Cannot find user.");
                } else {
                    userRef.forEach(doc => {
                        let prevRides = doc.data().rides;
                        let updateRef = doc.ref;

                        prevRides.push(rideID);
                        updateRef.update({
                            rides:prevRides
                        });

                        let prevRiders = ride.riders;
                        updateRef = snapshot.ref;

                        prevRiders.push(driverEmail);
                        updateRef.update({
                            riders:prevRiders
                        });
                    });
                }
            }).catch(err => {
                console.log('Error getting UserDoc', err);
            });
        };
    }).catch(err => {
        console.log('Error getting RideDoc', err);
    });
};

let temp = async ()=>{
    // console.log(await create("sasho's place", [1, 2, 3 ,4],'tat022@ucsd.edu',new Date("12/1/2018"),5 ))
    // console.log(await get("tat022@ucsd.edu"))
    console.log(await join('pUMOjQ1f9IPdAiUAfnnB', 'apn028@ucsd.edu'));

    console.log(await join('tElABoOcM3OgFXdLauMI', 'apn028@ucsd.edu'));
};
temp();
// endRides();
module.exports = {create, get, join, display};
