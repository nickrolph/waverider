import {db} from'./db';
import moment from 'moment';
import {login} from './login.js';
/*
    This method is how we are parsing through all the rides to mark
    them as finished. We iterate through all users, mark them
    as done if they are past the date they were said to leave.
    
    We do this by going into the db and using asychronous calls. 
    We will call this in our "getRides" method every time so it's constantly
    being refreshed. This is non optimal, but it is our only choice currently.
*/
let endRides = async ()=>{
    await db.collection("rides")
    .where('isDone', '==', false)
    .get()
    .then((doc)=>{
        doc.forEach(async(docRef)=>{
            let time = docRef.data().date;

            if(moment(time).isBefore(moment(Date.now()).toISOString())){
                docRef.ref.update({
                    isDone:true
                })
            }
            else{
                docRef.ref.update({
                    isDone:false
                })
            }
        })
    })
}

/*
    To cancel a ride we need the rideId that we are trying to cancel and 
*/
let cancelRideDriver = async (id, userEmail)=>{
    userEmail = userEmail.toLowerCase();
    await db.collection('rides').doc(id).get()
    .then((doc)=>{
            doc.ref.update({    
                isDone: true
            })

    });
    
    await db.collections('users').where('email', "==", userEmail)
    .get()
    .then( (doc)=>{
        docRef.forEach((doc)=>{
            let rides =doc.data().rides;

            for(i =0 ; i< rides.length;i++){
                if(rides[i] === id){   
                    rides.splice(i,1);
                }
            }
            doc.ref.update({
                rides:rides
            })
        });
    });
};

let cancelRideRider= async (id, userEmail)=>{
    userEmail = userEmail.toLowerCase();
    await db.collection('rides').doc(id).get()
    .then((doc)=>{
        let riders = doc.data().riders;
        for(let i = 0; i< riders.length; i++){
            if(riders[i] === userEmail){
                riders.splice(i,1);
            }
        }
        doc.ref.update({
            riders:riders
        })

    });
    
    await db.collections('users').where('email', "==", userEmail)
    .get()
    .then( (doc)=>{
        docRef.forEach((doc)=>{
            let rides =doc.data().rides;

            for(i =0 ; i< rides.length;i++){
                if(rides[i] === id){   
                    rides.splice(i,1);
                }
            }
            doc.ref.update({
                rides:rides
            })
        });
    });
};

let joinRide = async (id, userEmail)=>{
    userEmail = userEmail.toLowerCase();
    await db.collection('rides').doc(id).get()
    .then((doc)=>{
        let riders = doc.data().riders;
        for(let i = 0; i< riders.length; i++){
            if(riders[i] === userEmail){
                riders.splic(i,1);
            }
        }
        doc.ref.update({
            riders:riders
        })

    });
}
export  {cancelRide, cancelRideRider, endRides};