const jwt = require('jsonwebtoken');

let setPassword = function(password){
    let salt = 'temp';
    let hash = crypto.pbkdf2Sync(password,salt,10000,512,'sha512').toString('hex');
    return hash;    
}

let validatePassword = (password, compare)=>{
    let salt = 'temp';
    const hash = crypto.pbkdf2Sync(password,salt,10000,512,'sha512').toString('hex');
    return hash === compare;
}

let generateJWT = (email, _id)=>{
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate()+60);

    return jwt.sign({
        email: email,
        id: _id,
        exp: parseInt(expirationDate.getTime()/1000,10),
    },'secret');
}

let toAuthJSON = (id, email) =>{
    return {
        _id: id, 
        email: email,
        token: generateJWT()
    }
}

module.exports = { setPassword, validatePassword, generateJWT, toAuthJSON}
