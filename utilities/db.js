// const firebase = require('firebase');
import firebase from 'firebase';
require("firebase/firestore");
firebase.initializeApp({
    apiKey: "AIzaSyBgIjsSb5HgnIyFKEFqHniMWvNhRKdwmNs",
    authDomain: "waverider-e5689.firebaseapp.com",
    projectId: "waverider-e5689"
  });

let db = firebase.firestore();
db.settings({
    timestampsInSnapshots: true
  });

// module.exports = {db};
export {db};