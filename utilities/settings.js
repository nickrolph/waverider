import { db } from "./db";
// const {db} = require('./db');

/*
    Updates a user's password. The old password must be provided for verification as well.

    Returns true if the password was successfully changed. False otherwise.
 */
let updatePassword = async (email, newPassword, oldPassword) => {
  email = email.toLowerCase();
  let fair = await db
    .collection("users")
    .where("email", "==", email)
    .get()
    .then(function(userRef) {
      let temp;
      if (userRef.empty) {
        console.log("Cannot find user.");
        temp = false;
        return false;
      } else {
        userRef.forEach(doc => {
          let updateRef = doc.ref;
          // Check that the old password matches the current password.
          if (oldPassword === doc.data().password) {
            temp = true;
            updateRef.update({
              password: newPassword
            });
            return true;
          } else {
            temp = false;
            console.log("Incorrect old password.");
            return false;
          }
        });
        return temp;
      }
      return temp;
    })
    .catch(e => console.log(e));
  return fair;
};

/*
    Updates a user's phone number.
 */
let updatePhoneNumber = async (email, newPhoneNumber) => {
  email = email.toLowerCase();
  db.collection("users")
    .where("email", "==", email)
    .get()
    .then(async userRef => {
      if (userRef.empty) {
        console.log("Cannot find user.");
        return false;
      } else {
        userRef.forEach(doc => {
          let updateRef = doc.ref;
          updateRef.update({
            phoneNumber: newPhoneNumber
          });
        });
      }
    });
  return;
};

/*
    Updates a user's car information. Make sure the order is correct.
 */
let updateCarInfo = async (
  email,
  newColor,
  newMake,
  newModel,
  newNumSeats,
  newPlateNum
) => {
  email = email.toLowerCase();
  db.collection("users")
    .where("email", "==", email)
    .get()
    .then(async userRef => {
      if (userRef.empty) {
        console.log("Cannot find user.");
        return false;
      } else {
        userRef.forEach(doc => {
          let updateRef = doc.ref;

          updateRef.update({
            "carInfo.color": newColor,
            "carInfo.make": newMake,
            "carInfo.model": newModel,
            "carInfo.numSeats": newNumSeats,
            "carInfo.plateNum": newPlateNum
          });
        });
      }
    });
};

let temp = async () => {
  // console.log(await updatePassword('wlxu@ucsd.edu', 'new', 'hi'));
  // console.log(await updatePassword('wlxu@ucsd.edu', 'new', 'oops'));
  // console.log(await updatePhoneNumber('wlxu@ucsd.edu', '5106969'));
  // console.log(await updateCarInfo('wlxu@ucsd.edu', 'red', 'tesla','model 3', 69., 'aybabywsup'));
};
// temp();

export default { updatePassword, updateCarInfo, updatePhoneNumber };
