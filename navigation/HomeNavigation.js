import React from 'react';
import {
        StyleSheet,
        Image,
        SafeAreaView,
        ScrollView,
        View,
        Text
} from 'react-native';
import {
    createDrawerNavigator,
    createAppContainer,
    DrawerItems,
} from 'react-navigation';
import  {
    Icon
} from 'react-native-elements';

import LoginComponent from '../components/Login';
import RegisterComponent from '../components/RegisterAccount';
import HomeComponent from '../components/Home';
import ETAFormComponent from '../components/ETAForm';
import DriverInfoComponent from '../components/DriverInfo';
import ProfilePictureComponent from '../components/ProfilePicture';
import SettingsComponent from '../components/Settings';
import PromptDriverInfoComponent from '../components/promptDriverInfo';
import PastRidesComponent from '../components/PastRides';
import FAQComponent from '../components/FAQ';

const CustomDrawerContentComponent = (props) => (
    <SafeAreaView style={styles.container}>
        <View style={styles.profilePicContainer}>
            <Image source={require('../assets/WaveRider_Logo.png')} style={styles.profilePic}/>
            <Text style={styles.title}>WaveRider</Text>
        </View>
        <ScrollView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
            <DrawerItems {...props} />
        </ScrollView>
    </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
  profilePicContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 150
  },
  profilePic: {
    height: 120,
    width: 120,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold'
  }
});

const LoginStackNav = createDrawerNavigator(
    {
        Home: {
            screen: HomeComponent,
            navigationOptions: {
                drawerLabel: 'Current Rides',
                drawerIcon:
                    <Icon
                        name='home'
                        type='entypo'
                        color='black'
                    />
            }
        },

        PastRides: {
            screen: PastRidesComponent,
            navigationOptions: {
                drawerLabel: 'Past Rides',
                drawerIcon:
                    <Icon
                        name='automobile'
                        type='font-awesome'
                        color='black'
                    />
            }
        },

        FAQ: {
          screen: FAQComponent,
          navigationOptions: {
              drawerLabel: 'FAQ',
              drawerIcon:
                  <Icon
                      name='star'
                      type='font-awesome'
                      color='black'
                  />
          }
        },
        Settings: {
            screen: SettingsComponent,
            navigationOptions: {
                drawerLabel: 'Settings',
                drawerIcon:
                    <Icon
                        name='md-settings'
                        type='ionicon'
                        color='black'
                    />
            }
        },
        Login: {
            screen: LoginComponent,
            navigationOptions: {
                drawerLabel: 'Logout',
                drawerIcon:
                    <Icon
                        name='log-out'
                        type='entypo'
                        color='black'
                    />
            }
        }
    },
    {
        initialRouteName : 'Home',
        contentComponent: CustomDrawerContentComponent
    }
);

const AppContainer = createAppContainer(LoginStackNav);

export default AppContainer;
