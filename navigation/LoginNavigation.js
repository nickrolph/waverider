import React from 'react';
import { StyleSheet } from 'react-native';

import {
    createStackNavigator,
    createAppContainer,
    DrawerItems,
    SafeAreaView,
    ScrollView
} from 'react-navigation';

import LoginComponent from '../components/Login';
import RegisterComponent from '../components/RegisterAccount';
import HomeComponent from '../navigation/HomeNavigation';
import ETAFormComponent from '../components/ETAForm';
import DriverInfoComponent from '../components/DriverInfo';
import ProfilePictureComponent from '../components/ProfilePicture';
import RiderFormComponent from '../components/RiderForm';
import DriverFormComponent from '../components/DriverForm';
import ViewRideComponent from '../components/ViewRide';
import PromptDriverInfoComponent from '../components/promptDriverInfo'
import MatchedRideComponent from '../components/MatchedRide';

const LoginStackNav = createStackNavigator(
    {
        Login: {
            screen: LoginComponent
        },

        RegisterAccount: {
            screen: RegisterComponent
        },

        DriverInfo: {
            screen: DriverInfoComponent
        },
        PromptDriverInfo: {
            screen: PromptDriverInfoComponent
        },
        Home: {
            screen: HomeComponent
        },

        ProfilePicture: {
            screen: ProfilePictureComponent
        },

        ETAForm:{
            screen: ETAFormComponent
        },

        RiderForm: {
            screen: RiderFormComponent
        },

        DriverForm: {
            screen: DriverFormComponent
        },

        ViewRide: {
            screen: ViewRideComponent
        },

        MatchedRide: {
            screen: MatchedRideComponent
        }
    },
    {
        headerMode: 'none'
    }
);

const AppContainer = createAppContainer(LoginStackNav);

export default AppContainer;
